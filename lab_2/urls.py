from django.urls import path
from .views import xml, index, json

urlpatterns = [
    path('', index, name='index'), #home lab2
    # TODO Add friends path using friend_list Views
    #path return elemen untuk dimasukkan dalam urlpatterns di urls.py
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]