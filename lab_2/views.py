from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse # TODO
from django.core import serializers # TODO


# Create your views here.
def index(request): #req ke lab2 html
    new_note = Note.objects.all().values()  # TODO Implement this
    response = {'new_note': new_note}
    return render(request, 'lab2.html', response)

def xml(request):                           # TODO
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):                           # TODO
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")