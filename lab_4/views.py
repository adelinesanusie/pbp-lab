from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    new_note = Note.objects.all().values()  # TODO Implement this
    response = {'new_note': new_note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    
    if request.method == "POST":
        # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect("/lab-4/")
  
    context['form']= form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    new_note = Note.objects.all()
    response = {'new_note': new_note}
    return render(request, 'lab4_note_list.html', response)
