1. Apakah perbedaan antara JSON dan XML?<br/>
JSON (JavaScript Object Notation) merupakan format untuk menyimpan dan mentransfer data<br/>
XML (Extensible Markup Language) merupakan bahasa markup yang dirancang untuk menyimpan data<br/>
JSON dan XML memiliki beberapa segi perbedaan yaitu:<br/>
-Bahasa: JSON merupakan format data yang ditulis dalam JavaScript, sedangkan XML merupakan bahasa markup<br/>
-Penyimpanan data: data JSON disimpan seperti map dengan key beserta value, sedangkan data XML disimpan sebagai tree structure<br/>
-Pengolahan: JSON tidak melakukan proses atau perhitungan, sedangkan XML dapat melakukan proses atau pemformatan dokumen serta objek<br/>
-Kecepatan: JSON memiliki ukuran file yang sangat kecil sehingga data ditransfer dengan sangat cepat karena penguraiannya lebih cepat oleh JavaScript, sedangkan XML mentransmisi data lebih lambat karena penguraiannya besar dan lambat<br/>
-Namespaces: JSON tidak ada ketentuan untuk namespaces, sedangkan XML mendukung namespaces (komentar dan metadata)<br/>
-Array: JSON mendukung array yang dapat diakses, sedangkan XML tidak mendukung array secara langsung (jika ingin menggunakan array harus menambah tag di setiap item)<br/>
-Tipe data: JSON mendukung string, angka, array booolean, dan objek bertipe primitif, sedangkan XML mendukung banyak tipe data kompleks yaitu bagan, charts, dan data bertipe non-primitif<br/>
-Dukungan UTF: JSON mendukung UTF dan ASCII, sedangkan XML mendukung pengkodean UTF-8 dan UTF-16<br/>
-Ekstensi file: JSON diakhiri dengan ekstensi .json, sedangkan XML diakhiri dengan ekstensi .xml<br/>
<br/>
2. Apakah perbedaan antara HTML dan XML?<br/>
HTML (Hypertext Markup Language) merupakan bahasa markup untuk membuat struktur halaman website<br/>
XML (Extensible Markup Language) merupakan bahasa markup yang dirancang untuk menyimpan data<br/>
HTML dan XML memiliki beberapa segi perbedaan yaitu:<br/>
-Fungsi: HTML digunakan untuk desain halaman web yang di-render di client side, sedangkan XML digunakan untuk mentrasfer data antara aplikasi dan database<br/>
-Sifat: HTML statis, sedangkan XML dinamis<br/>
-Bahasa: HTML tidak case sensitive (tidak peka terhadap huruf kecil maupun huruf kapital), sedangkan XML case sensitive (peka terhadap huruf kecil maupun huruf kapital)<br/>
-Data: HTML tentang menampilkan data, sedangkan XML tentang menggambarkan data<br/>
-Dasar: HTML merupakan bahasa markup standar, sedangkan XML menyediakan framework untuk menentukan bahasa markup<br/>
-Struktural: HTML tidak mengandung informasi struktural, sedangkan XML menyediakan informasi<br/>
-Tag penutup: pada HTML tag penutup bersifat opsional, sedangkan pada XML harus menggunakan tag penutup<br/>
-Ekstensi file: HTML diakhiri dengan ekstensi .html, sedangkan XML diakhiri dengan ekstensi .xml<br/>
<br/>
Daftar Pustaka:<br/>
Risyan, Resa. 2020. Apa Perbedaan JSON Dan XML?. Diakses pada 26 September, dari https://www.monitorteknologi.com/perbedaan-json-dan-xml/<br/>
Nayoan, Aldwin. 2020. JSON: Pengertian, Fungsi dan Cara Menggunakannya. Diakses pada 26 September, dari https://www.niagahoster.co.id/blog/json-adalah/<br/>
Pamungkas, Ridandi Bintang. 2020. Panduan Lengkap XML: Pengertian, Contoh, dan Cara Membuka Filenya. Diakses pada 26 September, dari https://www.niagahoster.co.id/blog/xml/<br/>
Sawakinome. Perbedaan Antara JSON dan XML. Diakses pada 26 September, dari https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html<br/>
Iswara, Bagus Dharma. Perbedaan XML dan HTML: Fitur dan Kuncinya. Diakses pada 26 September, dari https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html<br/>
NATAPA. 2021. Perbedaan antara XML dan HTML. Diakses pada 26 September, dari https://id.natapa.org/difference-between-xml-and-html-2446
Aprilia, Putri. 2021. Pengertian HTML, Fungsi dan Cara Kerjanya. Diakses pada 26 September, dari https://www.niagahoster.co.id/blog/html-adalah/<br/>