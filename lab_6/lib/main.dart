import 'package:flutter/material.dart';
import 'package:lab_6/screens/home_screen.dart';
import 'package:lab_6/screens/login_screen.dart';
import 'package:lab_6/screens/rumahsakit_screen.dart';
import 'package:lab_6/screens/berita_screen.dart';
import 'screens/navbar_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ;
    return MaterialApp(
      title: 'Aplikasi Covid-19',
      theme: new ThemeData(
        primarySwatch: Colors.lightBlue,
        canvasColor: Color.fromRGBO(225, 245, 254, 1),
      ),
      home: const NavBar(),
      // body: Image.asset('asset/image/corona.jpg')
      routes: {
        HomeScreen.routeName: (ctx) => const HomeScreen(),
        RumahSakitScreen.routeName: (ctx) => const RumahSakitScreen(),
        BeritaScreen.routeName: (ctx) => const BeritaScreen(),
        LoginScreen.routeName: (ctx) => const LoginScreen(),
      },
    );
  }
}
