import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/login_screen';
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text('Login \n',
                  style: TextStyle(
                      color: Colors.indigo,
                      fontWeight: FontWeight.bold,
                      fontSize: 50)),
              const SizedBox(
                width: 400,
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: TextField(
                    style: TextStyle(color: Colors.teal),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Username',
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 400,
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: TextField(
                    style: TextStyle(color: Colors.teal),
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
              ),
              RaisedButton(
                child: const Text("OK"),
                onPressed: () {},
                color: Colors.indigo,
              )
            ],
          ),
        ));
  }
}
