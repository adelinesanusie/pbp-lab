import 'package:flutter/material.dart';

class BeritaScreen extends StatelessWidget {
  static const routeName = '/berita_screen';
  const BeritaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Berita"),
        ),
        body: Container(
          width: 600,
          height: 400,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            color: Colors.lightGreen[200],
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const <Widget>[
                ListTile(
                  leading: Icon(Icons.sick_rounded, size: 100),
                  title: Text(
                      'Studi Terbaru Sebut Vaksin Corona Ini Paling Manjur',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      )),
                  subtitle: Text(
                      'Jakarta, CNBC Indonesia - Sebuah studi terbaru merilis data mengenai vaksin Covid-19 dengan tingkat keampuhan tertinggi. Dalam penelitian itu, vaksin Covid-19 buatan Pfizer/BioNTech menjadi produk vaksin yang disebut ampuh menangkal virus corona. Mengutip Straits Times, hasil keampuhan Pfizer ini didapatkan saat dilakukan uji dengan tiga vaksin lainnya yakni Sinopharm, Sputnik V, dan AstraZeneca. Uji ini dilakukan kepada 196 reponden di Mongolia. Konsentrasi antibodi relatif rendah dirangsang oleh vaksin Sinopharm dan Sputnik V, tingkat menengah untuk vaksin AstraZeneca, dan nilai tertinggi untuk vaksin Pfizer/BioNTech, ujar studi yang dirilis di Jurnal Cell Host and Microbe itu.',
                      style: TextStyle(color: Colors.black, fontSize: 15)),
                ),
              ],
            ),
          ),
        ));
  }
}
