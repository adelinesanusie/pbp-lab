from django.urls import path
from .views import friend_list, index

urlpatterns = [
    path('', index, name='index'),                  #'' home panggil fungsi index
    # TODO Add friends path using friend_list Views
    #path return elemen untuk dimasukkan dalam urlpatterns di urls.py
    path('friends', friend_list, name='friends')    #'friends' panggil fungsi friend_list
]
