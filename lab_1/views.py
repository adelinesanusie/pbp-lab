from django.shortcuts import render
from datetime import datetime, date
from .models import Friend              # TODO baru

mhs_name = 'Adeline Sonia Sanusie'      # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 12, 16)         # TODO Implement this, format (Year, Month, Date)
npm = 2006595993                        # TODO Implement this


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    #nanti loop ada di friend_list_lab1.html buat tampilin friends
    #friends object Friend
    #object Friend punya instance variable nama npm DOB
    #instance variable dipanggil untuk setiap object yang ada
    #friends merupakan Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
