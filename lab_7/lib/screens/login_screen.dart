import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/login_screen';
  LoginScreen({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  TextEditingController name = TextEditingController();

  showAlertDialog(BuildContext context) {
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen()),
        );
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text('Your Account Has Been Created Successfully!'),
      backgroundColor: const Color.fromRGBO(190, 198, 222, 1),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Login"),
        ),
        body: Container(
          width: double.infinity,
          child: Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //IMAGE
                const Image(
                  image: AssetImage('../assets/cat.png'),
                  width: 300,
                  height: 300,
                ),

                //TITLE LOGIN
                const Text('LOGIN',
                    style: TextStyle(
                        color: Color.fromRGBO(132, 146, 193, 1),
                        fontWeight: FontWeight.bold,
                        fontSize: 50)),

                //USERNAME
                SizedBox(
                  width: 400,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: TextFormField(
                      style: const TextStyle(color: Colors.indigo),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Username',
                        icon: Icon(Icons.emoji_people_rounded),
                      ),
                      // validator: (value) {
                      //   if (value != null && value.isEmpty) {
                      //     print('You must fill the Username!');
                      //   }
                      //   return null;
                      // },
                    ),
                  ),
                ),

                //PASSWORD
                SizedBox(
                  width: 400,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: TextFormField(
                      style: const TextStyle(color: Colors.indigo),
                      obscureText: true,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                        icon: Icon(Icons.password_rounded),
                      ),
                      // validator: (value) {
                      //   if (value != null && value.isEmpty) {
                      //     print('You must fill the Password!');
                      //   }
                      //   return null;
                      // },
                    ),
                  ),
                ),

                //FORGOT PASSWORD
                const Text(
                  '\n Forgot Password? \n',
                  style: TextStyle(color: Colors.indigo, fontSize: 18),
                ),

                //BUTTON
                // ignore: deprecated_member_use
                RaisedButton(
                  child: const Text(
                    'LOGIN',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  color: const Color.fromRGBO(132, 146, 193, 1),
                  onPressed: () {
                    showAlertDialog(context);
                  },
                )
              ],
            ),
          ),
        ));
  }
}
